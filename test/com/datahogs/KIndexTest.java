package com.datahogs;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.datahogs.server.KIndex;

public class KIndexTest {

	public KIndexTest() {
		// test input to the query class
	}
	
	public static void run() throws FileNotFoundException {
		KIndex k=KIndex.getInstance();
		//String path = "/home/odeegan/workspace/Indexer/bin/document_sources";
		String path = "C:\\Users\\user\\Documents\\_School\\cecs429\\Test Data\\time_machine.txt";
		Scanner sc = new Scanner(new File(path));
		List<String> vocabulary = new ArrayList<String>();
		int count=0;

		sc = new Scanner(new File(path));
		 while(sc.hasNext()){
			 String word = sc.next(); 
			 word = word.replaceAll("[ ?,;:!.\"'()--_]", "");
			 if(!word.isEmpty() && !vocabulary.contains(word))
			 {
				
				 System.out.println(word);
				 vocabulary.add(word);
				 System.out.println(k.getGrams(word, 1));
				 System.out.println(k.getGrams(word, 2));
				 System.out.println(k.getGrams(word, 3));
				 count++;
			 }
				 
		 }
		
		 System.out.println("total words:" + count);
	}

}
