package com.datahogs;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.datahogs.server.KIndex;


/**
 * 
 * @author odeegan
 * 
 * This is the main testing class. It drives all the individual testing classes.
 * I'll post information about how to log errors shortly. Stay tuned.... 
 */

public class Tester {

	QueryTest queryTest;
	
	public static void main(String[] args) throws FileNotFoundException {
		// test the query class
		//print("QUERY TEST");
		//QueryTest.run();
		
		// test the porter class
		//print("PORTER TEST");	
		//PorterTest.run();
		
		// test the kgram indexer				Joanne: 10/20/12
		//print("KGRAM INDEX TEST");
		//KIndexTest.run();
		// test the kgram indexer				Joanne: 10/20/12
		//print("HEAP SORT TEST");
		//HeapSortTest.run();
		print("EDIT DISTANCE");
		CandidateList.run();
		 
	}
	public static void print(String label)
	{
		System.out.println("\n\n--------------------");
		System.out.println(label);
		System.out.println("--------------------");	
	}

}
