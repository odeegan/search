package com.datahogs.shared;

import java.io.Serializable;
import java.util.ArrayList;

public class ResultsContainer implements Serializable {

	ArrayList<Result> results = new ArrayList<Result>();
	
	int total = 0;
	String query = "";
	String expandedQuery = "";
	String queryProcessingTime = "";
	int currPos;
	int prevPos;
	
	public ResultsContainer() {}
	
	
	public ArrayList<Result> getResults() {
		return results;
	}
	
	public void addResult(Result result) {
		if (results == null) {
			results = new ArrayList<Result>();
			results.add(result);
		} else {
			results.add(result);
		}
	}
		
	public int getCount() {
		return results.size();
	}

	public void setTotal(int t) {
		total = t;
	}
	
	public int getTotal() {
		return total;
	}
	
	public void setCurrPos(int c) {
		currPos = c;
	}
	
	public int getCurrPos() {
		return currPos;
	}
	
	public void setPrevPos(int p) {
		prevPos = p;
	}
	
	public int getPrevPos() {
		return prevPos;
	}
	
	public void setQueryProcessingTime(String time) {
		queryProcessingTime = time;
	}
	
	public String getQueryProcessingTime() {
		return queryProcessingTime;
	}

	public void setQuery(String q) {
		query = q;
	}
	
	public void setExpandedQuery(String q) {
		expandedQuery = q;
	}
	
	public String getQuery() {
		return query;
	}
	
	public String getExpandedQuery() {
		return query;
	}	
}
