package com.datahogs.shared;

import com.datahogs.client.ResultHTML;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.user.client.ui.HTML;


public class Result implements IsSerializable {


	int id;
	String snippet = "";
	String link = "";
	double score = 0.0;

	public Result() {}
	

	public void setId(int id) {
		this.id = id;
	}
	
	public void setSnippet(String snippet) {
		this.snippet = snippet;
	}	
	
	public void setLink(String link) {
		this.link = link;
	}
	
	public void setScore(double score) {
		this.score = score;
	}
	
	public String getScore() {
		NumberFormat fmt = NumberFormat.getDecimalFormat();
	    return  NumberFormat.getFormat("0.##").format(score);

	}
	
	public int getId() {
		return id;
	}
	
	public String getSnippet() {
		return snippet;
	}
	
	public String getLink() {
		return link;
	}
	
	public HTML toHTML() {
		ResultHTML html = new ResultHTML();
		String str = "";
		html.setId(id);
		str += "<div class=\"result\"><div class=\"snippet\">" + snippet + "</div>";
		str += "<div class=\"id\">ID: " + id 
				+ "<div class=\"score\">SCORE: " + getScore() + "</div>" 
				+ "</div></div>";
		html.setHTML(str);
		return html;
	}
	
}