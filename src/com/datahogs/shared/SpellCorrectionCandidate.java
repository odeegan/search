package com.datahogs.shared;

import com.datahogs.client.SpellCorrectionCandidateHTML;
import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.user.client.ui.HTML;


public class SpellCorrectionCandidate implements IsSerializable {


	String term;
	int editDistance = 0;
	
		
	public SpellCorrectionCandidate() {}
	
	public void setTerm(String term) {
		this.term = term;
	}
	
	public String getTerm() {
		return term;
	}		
	
	public void setEditDistance(int i) {
		editDistance = i;
	}
	
	public int getEditDistance() {
		return editDistance;
	}
	
	public HTML toHTML() {
		SpellCorrectionCandidateHTML html = new SpellCorrectionCandidateHTML();
		String str = "";
		html.setTerm(term);
		str += "<div class=\"result\"><div class=\"editDistance\">Edit Distance: " 
				+ editDistance 
				+ "</div>";
				str += "<div class=\"candidate\">" + term + "</div></div>";
		html.setHTML(str);
		return html;
	}
	
}