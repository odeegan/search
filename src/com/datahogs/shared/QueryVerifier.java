package com.datahogs.shared;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;



public class QueryVerifier {

	/**
	 * Verifies that a query string is valid
	 * 
	 * @param query string text
	 * @return true if valid, false if invalid
	 */
	public static void validateQuery(String query) throws IllegalArgumentException {
		String message = "";
		Boolean valid = true;
		
		// throw an error immediately, if we have a bad character
		if(query.matches("[^a-zA-Z-+\"\']+")) {
			valid = false;
			message = "query contains invalid characters";
		}
		
		// check for an uneven number of quotes
		int quote_counter = 0;
		char[] chars = query.toCharArray();
		for (int i = 0; i < chars.length; i ++) {
			if (chars[i] == '"' ) {
				quote_counter = quote_counter + 1;
			}
		}
		if (quote_counter % 2 != 0) {
			valid = false;
			message = "there is an open quote";
		}
		
		
		if (!valid) {
			throw new IllegalArgumentException(message);	
		}
		
		
		String[] Qs = query.split("\\+");
		for (String Q:Qs) {
			List<String> QueryLiterals = new ArrayList<String>();


			// split into QueryLiterals by splitting on white space, but
			// preserve white spaces in quoted strings			
			RegExp pattern = RegExp.compile("-?\\w+-?\\w*|-?\"[\\w\\s]*\"", "g");

			for (MatchResult result = pattern.exec(Q); result != null; result = pattern.exec(Q)) {
				   //System.out.println(result.getGroup(0));
				   QueryLiterals.add(result.getGroup(0));
				}

			int neg_count = 0;
			for (String QL: QueryLiterals) {
				QL = QL.trim();
				// check for only non positive query literals
				if (QL.matches("^\\s*-(\\w+-?\\w*|\"\\S+\")$")) {
					neg_count = neg_count +1;
				}
			}
					
			if (neg_count == QueryLiterals.size() && QueryLiterals.size() != 0) {
				valid = false;
				message = "each Qi just have at least one positive query literal";
			}
			
			if (!valid) {
				throw new IllegalArgumentException(message);
			
			}

		}
	}
}
