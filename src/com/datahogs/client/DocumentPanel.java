package com.datahogs.client;

import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.ScrollPanel;

class DocumentPanel extends ScrollPanel {


	public DocumentPanel() {
        sinkEvents(Event.ONKEYDOWN);
	}
	
	
	public void onBrowserEvent(Event event) {
		switch (DOM.eventGetType(event)) {
	       	case Event.ONKEYDOWN:
	       		if (DOM.eventGetKeyCode(event) == KeyCodes.KEY_ESCAPE) {
	       			this.hide();
	       		}
	       	
            /*
             * Stop the event happening where it usually would. If we don't
             * do this, then the panel this widget is in would scroll if it
             * could
             */
            DOM.eventPreventDefault(event);
            break;	    
	    }
	}
	
	
	public void show() {
		this.setVisible(true);
		System.out.println("over here!");
	}
	
	
	public void hide() {
		System.out.println("you can't see me!");
		this.setVisible(false);
	}
}
