package com.datahogs.client;

import java.util.ArrayList;

import com.datahogs.shared.ResultsContainer;
import com.datahogs.shared.SpellCorrectionCandidate;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("query")
public interface QueryService extends RemoteService {
	String getBlobstoreUploadUrl();
	String createIndex();
	ResultsContainer queryServer(String name) throws IllegalArgumentException;
	ResultsContainer next();
	ResultsContainer previous();
	String getDocumentTextById(int id);
	ArrayList<SpellCorrectionCandidate> suggestSpelling(String name) throws IllegalArgumentException;
}
