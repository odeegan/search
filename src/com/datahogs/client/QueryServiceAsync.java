package com.datahogs.client;

import java.util.ArrayList;

import com.datahogs.shared.ResultsContainer;
import com.datahogs.shared.SpellCorrectionCandidate;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>QueryService</code>.
 */
public interface QueryServiceAsync {
    void getBlobstoreUploadUrl(AsyncCallback<String> callback);
	void createIndex(AsyncCallback<String> callback);
	void queryServer(String input, AsyncCallback<ResultsContainer> callback)
			throws IllegalArgumentException;
	void next(AsyncCallback<ResultsContainer> callback);
	void previous(AsyncCallback<ResultsContainer> callback);
	void getDocumentTextById(int id, AsyncCallback<String> callback);	
	void suggestSpelling(String term, AsyncCallback<ArrayList<SpellCorrectionCandidate>> callback);

}
