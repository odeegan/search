package com.datahogs.client;

import com.google.gwt.user.client.ui.HTML;

public class ResultHTML extends HTML {

	int id;
	double score;
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setScore(double score) {
		this.score = score;
	}
	
	public int getId() {
		return this.id;
	}
	
	public double getScore() {
		return this.score;
	}
	
}
