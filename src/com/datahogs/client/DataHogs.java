package com.datahogs.client;

import java.util.ArrayList;

import com.datahogs.shared.QueryVerifier;
import com.datahogs.shared.ResultsContainer;
import com.datahogs.shared.SpellCorrectionCandidate;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class DataHogs implements EntryPoint {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";
	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	private final QueryServiceAsync queryService = GWT
			.create(QueryService.class);
	private Button searchButton = new Button("Search");
	private TextBox searchField = new TextBox();	
	private Label errorLabel = new Label();	
	private VerticalPanel resultsPanel = new VerticalPanel();
	private final HTML indexDetailHTML = new HTML();
	private final HTML resultsDetailHTML = new HTML();
	private final HTML queryStringHTML = new HTML();

	private Button nextButton = new Button("Next");
	private Button previousButton = new Button("Previous");
	private Label paginatorLabel = new Label();
	
	private PopupPanel documentPopupPanel = new PopupPanel(false);
	private Button addDocumentsButton = new Button("Add Documents");
	
	private PopupPanel uploadPopupPanel = new PopupPanel(false);
	final Button uploadButton = new Button();
	final FormPanel uploadForm = new FormPanel();
	final FileUpload uploadField = new FileUpload();
		
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		searchField.setText("");
		searchField.setStylePrimaryName("searchField");
		
		// We can add style names to widgets
		searchButton.addStyleName("searchButton");

		
		// Add the searchField and searchButton to the RootPanel
		// Use RootPanel.get() to get the entire body element
		RootPanel.get("searchFieldContainer").add(searchField);
		RootPanel.get("searchButtonContainer").add(searchButton);
		RootPanel.get("resultsContainer").add(resultsPanel);
		RootPanel.get("errorLabelContainer").add(errorLabel);
		RootPanel.get("queryStringContainer").add(queryStringHTML);
		RootPanel.get("resultsDetail").add(resultsDetailHTML);
		RootPanel.get("indexDetail").add(indexDetailHTML);

		RootPanel.get("previousButton").add(previousButton);
		RootPanel.get("paginatorLabel").add(paginatorLabel);
		RootPanel.get("nextButton").add(nextButton);
		nextButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
				resultsPanel.clear();
                System.out.println("getting next set of results");
                queryService.next(new AsyncCallback<ResultsContainer>() {
    						public void onFailure(Throwable caught) {
    						// Show the RPC error message to the user			
    							resultsPanel.add(new HTML("Remote Procedure Call - Failure"));								
    						}

    						public void onSuccess(ResultsContainer resultsContainer) {
    							//setQueryString(resultsContainer.getQuery());
    							//setResultsDetail(resultsContainer.getCount(),
    							//resultsContainer.getQueryProcessingTime());
    							setQueryResults(resultsContainer);
    						}
    				}); 
            }
        });
		
		previousButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
				resultsPanel.clear();
                System.out.println("getting previous set of results");
                queryService.previous(new AsyncCallback<ResultsContainer>() {
    						public void onFailure(Throwable caught) {
    						// Show the RPC error message to the user			
    							resultsPanel.add(new HTML("Remote Procedure Call - Failure"));								
    						}

    						public void onSuccess(ResultsContainer resultsContainer) {
    							setQueryResults(resultsContainer);
    						}
    				}); 
            }
        });
		
		
		// don't display the paginator at first
		previousButton.setVisible(false);
		paginatorLabel.setVisible(false);
		nextButton.setVisible(false);
		
		// Focus the cursor on the name field when the app loads
		searchField.setFocus(true);
		searchField.selectAll();
		
		RootPanel.get("uploadContainer").add(addDocumentsButton);
		// Create a FormPanel and point it at a service.
		
		uploadField.getElement().setAttribute("multiple", "multiple");
	    
		VerticalPanel up = new VerticalPanel();
	    uploadForm.setWidget(up);
	    up.add(uploadField);
	    up.add(uploadButton);
        uploadButton.setText("Loading...");
        uploadForm.setEncoding(FormPanel.ENCODING_MULTIPART);
        uploadForm.setMethod(FormPanel.METHOD_POST);
        uploadButton.setEnabled(false);
        uploadField.setName("file");

        uploadButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                uploadForm.submit();
				resultsPanel.clear();
                System.out.println("form submitted");
            }
        });
        
        // Now we use out GWT-RPC service and get an URL
        startNewBlobstoreSession();

        // Once we've hit submit and it's complete, let's set the form to a new session.
        // We could also have probably done this on the onClick handler
        uploadForm.addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() {
            @Override
            public void onSubmitComplete(SubmitCompleteEvent event) {
                setIndexDetail("Indexing.......");
            	System.out.println("upload complete");
                //System.out.println(event.getResults());
                uploadForm.reset();
				uploadPopupPanel.hide();
				// instruct the query service to create the index
				queryService.createIndex(new AsyncCallback<String>() {
					public void onFailure(Throwable caught) {
						// Show the RPC error message to the user
						resultsPanel.clear();
						resultsPanel.add(new HTML("Remote Procedure Call - Failure<br/>" +
								"Failed to Create the Index"));
					}

					public void onSuccess(String indexTime) {
						setIndexDetail("Indexing completed in " + indexTime + " seconds");
					}
				});
				startNewBlobstoreSession();
            }
        });
		
		
		class AddDocumentsButtonHandler implements ClickHandler {
			/**
			 * Fired when the user clicks on the searchButton.
			 */
			public void onClick(ClickEvent event) {
				showUploadPopUp();
			}
		}
		
		addDocumentsButton.addClickHandler(new AddDocumentsButtonHandler());
		

		// Create a handler for the searchButton and nameField
		class SearchFieldHandler implements ClickHandler, KeyUpHandler {
			/**
			 * Fired when the user clicks on the searchButton.
			 */
			public void onClick(ClickEvent event) {
				sendQueryToServer();
			}

			/**
			 * Fired when the user types in the nameField.
			 */
			public void onKeyUp(KeyUpEvent event) {
				checkInput(searchField.getText());
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					resultsPanel.clear();
					sendQueryToServer();
				}
			}


			
			/**
			 * Send the name from the nameField to the server and wait for a response.
			 */
			private void sendQueryToServer() {
				// First, we validate the input.
				errorLabel.setText("");
				String textToServer = searchField.getText();
				
				// Then, we send the input to the server.
				//searchButton.setEnabled(false);
				resultsPanel.add(new HTML("waiting for results..."));
				
								
				queryService.queryServer(textToServer,
					new AsyncCallback<ResultsContainer>() {
						public void onFailure(Throwable caught) {
						// Show the RPC error message to the user			
							resultsPanel.clear();
							resultsPanel.add(new HTML("Remote Procedure Call - Failure"));								
						}

						public void onSuccess(ResultsContainer resultsContainer) {
							setQueryString(resultsContainer.getQuery());
							setResultsDetail(resultsContainer.getTotal(),
							resultsContainer.getQueryProcessingTime());
							setQueryResults(resultsContainer);
						}
				});
			}
		}

		// Add a handler to send the name to the server
		SearchFieldHandler searchFieldHandler = new SearchFieldHandler();
		searchButton.addClickHandler(searchFieldHandler);
		searchField.addKeyUpHandler(searchFieldHandler);
	}
	
	
	private void checkInput(String input) {
		try {
			QueryVerifier.validateQuery(input);	
		} catch (IllegalArgumentException e) {
			errorLabel.setText(e.getMessage());
			searchField.addStyleName("invalid");
			return;
		}
		searchField.removeStyleName("invalid");
		errorLabel.setText("");
	}
	
	private boolean isSingleTermSearch() {
		if (searchField.getText().split(" ").length > 1) {
			return false;
		}
		if (searchField.getText().contains("*")) {
			return false;
		}
		return true;
	}
	
	private void setQueryResults(ResultsContainer rc) {		
		if (rc.getResults().isEmpty()) {
			resultsPanel.clear();
			if (isSingleTermSearch()) {
				queryService.suggestSpelling(searchField.getText(),
						new AsyncCallback<ArrayList<SpellCorrectionCandidate>>() {
							public void onFailure(Throwable caught) {
							// Show the RPC error message to the user			
								resultsPanel.add(new HTML("Remote Procedure Call - Failure"));								
							}

							public void onSuccess(ArrayList<SpellCorrectionCandidate> candidates) {
								setSpellCorrectionResults(candidates);
							}
					});
			} else {
				resultsPanel.add(new HTML("<h2>No results found.</h2>"));
				return;			
			}

		} else {
			
			if (rc.getCurrPos() < rc.getTotal()) {
				nextButton.setVisible(true);
				paginatorLabel.setVisible(true);
			} else {
				nextButton.setVisible(false);
			}
			
			if (rc.getCurrPos() > 15) {
				previousButton.setVisible(true);
			} else {
				previousButton.setVisible(false);
			}
			
			paginatorLabel.setText( 
					(rc.getPrevPos() + 1)
					+  " - "
					+ (rc.getCurrPos())
					+ " of "
					+ rc.getTotal());
			
			// a handler to respond when a result is clicked on
			class ResultHandler implements ClickHandler {
				/**
				 * Fired when the user clicks on a result
				 */
				public void onClick(ClickEvent event) {
					// show the original document text
					// in the documentPanel
					ResultHTML sender = (ResultHTML)event.getSource();
				    int id = ((ResultHTML)sender).getId();
					showDocument(id);
				}
			}
			
			resultsPanel.clear();
			HTML html;
			for (int i=0; i < 15; i++) {
				html = rc.getResults().get(i).toHTML();
				html.addClickHandler(new ResultHandler());
				resultsPanel.add(html);
			}
		}
	}
	
	
	public void setSpellCorrectionResults(ArrayList<SpellCorrectionCandidate> candidates) {
		resultsPanel.clear();
		HTML html;
		for (SpellCorrectionCandidate c: candidates) {
			html = c.toHTML();
			resultsPanel.add(html);
		}
	}
	
	
	private void setIndexDetail(String time) {
		indexDetailHTML.setHTML(time);
	}
	
	
	private void setResultsDetail(int count, String time) {
		resultsDetailHTML.setHTML(count + " results (" + time +" secs)");
	}

	private void setQueryString(String queryString) {
		queryStringHTML.setHTML("searching for: " + queryString);
	}
	
	
	private void showDocument(int id) {
		queryService.getDocumentTextById(id, new AsyncCallback<String>() {
			public void onFailure(Throwable caught) {
				// Show the RPC error message to the user
				resultsPanel.clear();
				resultsPanel.add(new HTML("Remote Procedure Call - Failure<br/>" +
						"Failed to Create the Index"));
			}

			public void onSuccess(String document) {
				documentPopupPanel.setGlassEnabled(true);
				// Create a PopUpPanel with a button to close it
			    documentPopupPanel.setStylePrimaryName("popupPanel");
				//documentPopupPanel.setStyleName("popupPanel");
			    VerticalPanel panel = new VerticalPanel();
			    panel.setStyleName("popup-table");
			    //HTML idHTML = new HTML("<span class=\"id\">ID:</span>");
			    ScrollPanel scrollPanel = new ScrollPanel(new HTML(document));			    
				scrollPanel.setHeight("400px");
				scrollPanel.setWidth("700px");
				scrollPanel.setStyleName("popup-scrollPanel");
			    
			    class ShowPopup implements ClickHandler {
					/**
					 * Fired when the user clicks on a result
					 */
					public void onClick(ClickEvent event) {
						// show the original document text
						// in the documentPanel
						documentPopupPanel.hide();
					}
				}
			    
			    Button exitButton = new Button("Close");
			    exitButton.setStyleName("popup-button");
			    exitButton.addClickHandler(new ShowPopup());
			    SimplePanel buttonHolder = new SimplePanel();
			    buttonHolder.add(exitButton);
			    buttonHolder.setStyleName("popup-buttonHolder");
			    //documentPopupPanel.setWidget(scrollPanel);
			    panel.add(buttonHolder);
			    //panel.add(idHTML);
			    panel.add(scrollPanel);
			    documentPopupPanel.setWidget(panel);
				documentPopupPanel.show();
				documentPopupPanel.center();
				}
			});		
	}
	
	
	private void showUploadPopUp() {
		uploadPopupPanel.setGlassEnabled(true);
		// Create a PopUpPanel with a button to close it
	    uploadPopupPanel.setStylePrimaryName("uploadPopupPanel");
	    
	    class ShowPopup implements ClickHandler {
			/**
			 * Fired when the user clicks on a result
			 */
			public void onClick(ClickEvent event) {
				// show the original document text
				// in the documentPanel
				uploadPopupPanel.hide();
			}
		}
	    
	    VerticalPanel uploadPanel = new VerticalPanel();
	    
		uploadPopupPanel.setWidget(uploadPanel);
	    Button exitButton = new Button("Close");
	    exitButton.setStyleName("popup-button");
	    exitButton.addClickHandler(new ShowPopup());
	    SimplePanel buttonHolder = new SimplePanel();
	    buttonHolder.add(exitButton);
	    buttonHolder.setStyleName("popup-buttonHolder");
	    //documentPopupPanel.setWidget(scrollPanel);
	    uploadPanel.add(buttonHolder);
		uploadPanel.add(uploadForm);
		uploadPopupPanel.show();
		uploadPopupPanel.center();
		
	}
	
	
    private void startNewBlobstoreSession() {
        queryService.getBlobstoreUploadUrl(new AsyncCallback<String>() {

            public void onSuccess(String result) {
                //System.out.println("created new blobstore session");
            	uploadForm.setAction(result);
                uploadButton.setText("Upload");
                uploadButton.setEnabled(true);
            }

            public void onFailure(Throwable caught) {
            	System.out.println("failed to create new blob session");
            }
        }); 
    }
	
}
