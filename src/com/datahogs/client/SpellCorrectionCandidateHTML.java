package com.datahogs.client;

import com.google.gwt.user.client.ui.HTML;

public class SpellCorrectionCandidateHTML extends HTML{

	
	String term;
	int editDistance;
	
	public void setTerm(String term) {
		this.term = term;
	}
	
	public String getTerm() {
		return term;
	}
	
	public void setEditDistance(int i) {
		this.editDistance = i;
	}
	
	public int getEditDistance() {
		return editDistance;
	}
	
}
