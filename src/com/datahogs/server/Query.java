package com.datahogs.server;

import java.util.ArrayList;


public class Query {

	private ArrayList<Q> qList = new ArrayList<Q>();
	private String queryAsString = "";
	
	public Query(String queryString) {
		queryAsString = queryString.trim().toLowerCase();
		// split into query literals
		for (String q: queryString.split("\\+")) {
			// trim leading and trailing white space
			// and place in our list of Q(s)			
			System.out.println("\n\nQi: " + "'" + q.trim() + "'");
			qList.add(new Q(q.trim().toLowerCase()));
		}
	}
	
	public ArrayList<Q> getQList() {
		return qList;
	}

	
	/*
	 * returns a list of all positive query literals in the query
	 * this is used to help score the documents
	 */
    public ArrayList<QueryLiteral> getPositiveQueryLiterals() {
        ArrayList<QueryLiteral> qls = new ArrayList<QueryLiteral>();
        for (Q q:qList) {
            for (QueryLiteral ql: q.getQueryLiterals()) {
                if (!ql.isNegative()) {
                    qls.add(ql);                                    
                }
            }
        }
        return qls;
    }
	
    /*
     * return the computed TFtd * IDF score for the doc
     */
	public void scoreDoc(PListDoc doc) {
		double score = 0;
		// for each query literal
		for (QueryLiteral ql: getPositiveQueryLiterals()) {
			// for each term in the query literal
			for (String term: ql.getTerms()) {
				double termWeightedIDF = Index.getInstance().getTermWeightedIDF(term, doc.getId());
				//System.out.println("doc " + doc.getId() + " score " + twidf + "term: " + term );
				score = score + termWeightedIDF;		
			}
		}
		doc.setScore(score);
	}
	
	public PList getPostingsList() {
		PList result = null;
		PList plist = null;
		
		if (qList.isEmpty()) {
			result = new PList();
		} else {
			// merge all of the postingsLists from the list of Qs
			plist = qList.get(0).getPostingsList();
			for (int i = 1; i < qList.size(); i++) {
				plist = plist.merge(qList.get(i).getPostingsList());
			}
			// score each document against the query terms
			for (PListDoc doc: plist.getDocs()) {
				scoreDoc(doc);
			}
			result =  plist.deepCopy();					
		}
		return result;

	}
	
	public String toString() {
		return queryAsString;
	}
	
}
