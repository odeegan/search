package com.datahogs.server;

/*
 * This class holds all of the stemmed term related data.
 * When you query the index using a stemmed term, it returns a TermData object
 *  it contains
 *  - postingsList (an ArrayList of PostingsListDocuments)
 *  - term frequency
 *  - and anything else we need to keep track of for each term
 */
public class TermData {

	private PList postingsList = new PList();
	
	public TermData() {}
	
	public PList getPostingsList() {
		return postingsList;
	}
	
	public void addAppearance(int docId, int position) {
		Boolean found = false;
		for (PListDoc pld: this.postingsList.getDocs()) {
			if (pld.getId() == docId) {
				pld.positions.add(position);
				found = true;
			}
		}
		if (!found) {
			postingsList.add(new PListDoc(docId, position));
		}
	}
		
	public Double getIDF() {
		int numDocsIndexed = Index.getInstance().getNumberofDocumentsIndex();
		return Math.log(numDocsIndexed/getDocumentFrequency())/Math.log(2);
	}
		
	private int getDocumentFrequency() {
		return postingsList.getSize();
	}
	
}
