package com.datahogs.server;
import java.util.HashMap;


public class Index {

	private static final Index index = new Index();
	
	// this is our inverted document term index
	HashMap<String, TermData> invertedIndex = new HashMap<String, TermData>(); 
		
	int numberOfDocumentsIndexed;
	
	private Index() {}

	public static Index getInstance() {
		return index;
	}

	public void reset() {
		invertedIndex.clear();
	}
	
	public void setNumberOfDocumentsIndex(int num) {
		numberOfDocumentsIndexed = num;
	}
	
	public int getNumberofDocumentsIndex() {
		return numberOfDocumentsIndexed;
	}
	
	
	public void addTerm(String term, int docId, int position) {
		if (invertedIndex.containsKey(term)) {
			// add the term appearance to the existing TermData
			invertedIndex.get(term).addAppearance(docId, position);
		} else {
			// create a new instance of TermData
			TermData td = new TermData();
			td.addAppearance(docId, position);
			invertedIndex.put(term, td);
			// add the first appearance of the term to the new TermData
			//invertedIndex.get(term).addAppearance(docId, position);
		}
	}
	
	/*
	 * Returns the postingsList for the given stemmed term.
	 * The postingsList is an ArrayList of PostingsListDocuments
	 */
	public PList getPostingsList(String term) {
		if (invertedIndex.containsKey(term)) {
			return invertedIndex.get(term).getPostingsList();
		} else {
			// if we don't have the term, return an empty list
			System.out.println("index does not contain " + term);
			return new PList();
		}
	}
	
	public double getIDF(String term) {
		//System.out.println("get IDF for: " + term);
		if (invertedIndex.containsKey(term)) {
			return invertedIndex.get(term).getIDF();
		} else {
			// TODO: add error checking and fail safely
			System.out.println("index does not contain " + term);
			return 0;
		}
	}
	
	public int getTFtd(String term, int docID) {
		if (invertedIndex.get(term).getPostingsList().contains(docID)) {
			//System.out.println("get TFtd for Term: " + term + " docID: " + docID);
			//return 2;
			return invertedIndex.get(term).getPostingsList().getDocById(docID).getTermFrequency(); 			
		} else {
			return 0;
		}
	}
	
	public double getTermWeightedIDF(String term, int docID) {
		//System.out.println("get weighted IDF for Term: " + term + " docID: " + docID);
		return getIDF(term) * getTFtd(term, docID);
		//return getIDF(term);

	}
}
