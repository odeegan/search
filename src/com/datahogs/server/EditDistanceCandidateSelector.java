package com.datahogs.server;

import java.util.ArrayList;
import java.util.HashMap;

public class EditDistanceCandidateSelector {
	//CandidateList
	public HashMap<String, CandidateData> cList = new HashMap<String, CandidateData>();
	public EditDistanceCandidateSelector(){}
	
	/* u = misspelled word
	 * C = candidate list
	 * k=2
	 * find 2-grams of word not found in vocabulary
	 * find each term that has the ith kgram of u
	 * if term is not in C and hasn't been removed (how do we keep track?)
	 *     add it to C
	 *     find best(c) and worst(c)
	 *     b(c) = 2(min(|u|, |term|) - (i-1))
	 *     w(c) = |t| + |u| - 2
	 * else if term is in C, decrement w(c) by 2
	 * for every c in C that does not have the ith k-gram of u
	 *     increment b(c)
	 *     
	 */
	public void CalculateCandidateList(String u, int k) {
		//find 2-grams of word not found
		ArrayList<String> missingWord = getGram(u,k);
		ArrayList<String> removedWords  = new ArrayList<String>();
		HashMap<String, CandidateData> C = new HashMap<String, CandidateData>();
		int i=0;
		int minWorstCase = 100;
		//find each term that has ith kgram of u
		for(String gram : missingWord) {	
			if(KIndex.getInstance().getKGramIndex().get(gram) != null){
				for(String word:KIndex.getInstance().getKGramIndex().get(gram)){
					
					//term is not in C, add to C and find best(c) & worst(c)
					if(!C.containsKey(word)){
						C.put(word, new CandidateData( bestCase(u,word,i, gram), worstCase(u,word), gramPosition(gram, word), EditDistance.editDistanceCount(u, word) ));	
					}
					else{ 
						//term is in C, decrement w(c) by 2
						C.get(word).decWorstCase();
					}

				}
			}
			//for every c in C that does not have ith k-gram of u, increment b(c)
			for(String c: C.keySet()){
				// find 2-grams of suggested word in candidate list
				ArrayList<String> suggestedWord = getGram(c, k);
				// increment the best case if the position of suggested word is less than suggested word length
				// and if the word does not have the ith k-gram of u, increment b(c)

				if(  C.get(c).getPosition()  < c.length() &&
					!suggestedWord.get(C.get(c).getPosition()).equals(missingWord.get(i)) ){
					C.get(c).incBestCase();
					//if(c.equals("machine"))
					//	System.out.println(C.get(c).getBestCase() + " " + C.get(c).getPosition());
				}
				// increment the k-gram position of the suggested word
				C.get(c).setPosition( C.get(c).getPosition()+1 );
			}
			i++;
			
		} 
		minWorstCase = findMinWorstCase(C);
		//display list of all candidates (not yet reduced)
		//System.out.println(minWorstCase);
		for(String c: C.keySet()){
			if(C.get(c).getEditDistance() <= 2)// &&
				//C.get(c).getBestCase() >= c.length()-1 && C.get(c).getBestCase() <= c.length()+1 &&
				//C.get(c).getWorstCase() >= c.length()-1 && C.get(c).getWorstCase() <= c.length()+1)
					{
					//do nothing
				//System.out.println(c + 
						//"\n min Best Case: " + C.get(c).getBestCase() + 
						//" \n min Worst Case: " + C.get(c).getWorstCase() + 
						//" \n min Edit Distance" + C.get(c).getEditDistance());
				cList.put(c, new CandidateData(C.get(c).getBestCase(), C.get(c).getWorstCase(), C.get(c).getPosition(), C.get(c).getEditDistance()));
			}
		}
	}
	
	public HashMap<String, CandidateData> getCandidateList(){
		return cList;
	}
	
	public static int findMinWorstCase(HashMap<String, CandidateData> cList){
		int min=100;
		for(String c: cList.keySet()){
			if(cList.get(c).worstCase < min)
				min = cList.get(c).worstCase;
		}
		return min;
	}
	//-------------------------------------------------------------
	//find k-grams of a word
	public static ArrayList<String> getGram(String u, int k){
		ArrayList<String> kgrams = new ArrayList<String>();
		String word="$"+u+"$";
		for(int i=0; i < word.length()-k + 1; i++) {
			kgrams.add(word.substring(i,i+k));
		}
		return kgrams;
	}
	
	//-------------------------------------------------------------
	//b(c) = 2(min(|u|, |term|) - (i-1))
	//I changed it to |t| + |u| - 2(min(|u|, |t|))	
	// |t| − |u| + 2i − 2
	public static int bestCase(String u, String t, int i, String gram) {
		int union = u.length() - t.length();
		int bCount = ( Math.abs(union) + 2 * i);
		//int bCount = ( (u.length() + t.length() + 2) - 2 * min(u.length()+1 - i, t.length()+1 - gramPosition(gram, t)));
		return bCount;			 
	}
	
	//-------------------------------------------------------------
	//w(c) = |t| + |u| - 2
	public static int worstCase(String u, String t) {
		int wCount = t.length() + u.length();
		return wCount;
	}
	
	//-------------------------------------------------------------
	//index position of suggested word where the first matched gram appears
	public static int gramPosition(String gram, String t) {
		int pos=0;
		String word = "$" + t + "$";
		for(pos=0; pos < word.length()-2 + 1; pos++) {
			if(word.substring(pos,pos+2).equals(gram))
				return pos;
		}
		return pos;
	}
	//-------------------------------------------------------------
	//compares two integer values and 
	// returns the smallest value
	public static int min(int dist1, int dist2) {
		if(dist1 < dist2)
			return dist1;
		else
			return dist2;
	}
	

}
