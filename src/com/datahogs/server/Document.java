package com.datahogs.server;

public class Document {

	private Integer id;
	private String text = null;
	private String snippet = null;
	
	public Document(Integer id, String text) {
		this.text = text;
		this.id = id;
	}
	
	public Integer getId() {
		return this.id;
	}
		
	public String getText() {
		return text;
	}
	
	public void setSnippet(String s) {
		snippet = s;
	}
	
	public String getSnippet() {
		return snippet;
	}
}
