/*
 * Author: Joanne Co
 */
package com.datahogs.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class KIndex {

	private static final KIndex kindex = new KIndex();

	HashMap<String, List<String>> kinvertedIndex = new HashMap<String, List<String>>(); 
	HashSet<String> wordList = new HashSet<String>();

	private KIndex() {}

	public static KIndex getInstance() {
		return kindex;
	}
	
	public void reset() {
		kinvertedIndex.clear();
		wordList.clear();
	}
	
	public HashMap<String, List<String>>getKGramIndex(){
		return kinvertedIndex;
	}
	public int indexWord(String word, int k) {
		//check if word is in the list
		//create a list of k-grams
		//map every word to it's specified k-gram
		
		int count=0;
		if(!wordList.contains(word)){
			wordList.add(word);
			List<String> kgrams=new ArrayList<String>();
			List<String> kList=new ArrayList<String>();
			for(int i=1; i<=k; i++){
				kList = getGrams(word, i);
				for(String kg: kList){
					kgrams.add(kg);
				}
			}
			addWordToIndex(word, kgrams);
			count++;
		}
		return count;
	}
	
	public List<String> getGrams(String word, int k){

		List<String> kGrams = new ArrayList<String>();
		if(word.length() == 1 && k ==3)
		{
			String gram= "$" + word + "$";
			kGrams.add(gram);
		}
		else{
			if(k !=1){
				String start = "$" + word.substring(0, k-1);
				kGrams.add(start);
				String end = word.substring( word.length()-k + 1 ) + "$";
				kGrams.add(end);
			}
			for(int i=0; i < word.length()-k + 1; i++)
			{
				kGrams.add(word.substring(i,i+k));
			}
		}
		return kGrams;
	}
	
	public void addWordToIndex(String word, List<String> kgrams){
		for(String kgram: kgrams){
			List<String> words = kinvertedIndex.get(kgram);
			if(words != null){
				words.add(word);
			}
			else{
				List<String> newWordList = new ArrayList<String>();
				kinvertedIndex.put(kgram, newWordList);
				words = kinvertedIndex.get(kgram);
				words.add(word);
			}
				
		}
		
	}
	 
}
