package com.datahogs.server;


public class Indexer {
	Index index;
	KIndex kindex;
	DocStore docStore;
	
	
	public Indexer(DocStore docStore, Index index, KIndex kindex) {
		this.index = index;
		this.kindex = kindex;
		this.docStore = docStore;
		indexDocuments();
		
		//-----------------------------------------------------------------------
		//for(String key : kindex.getKGramIndex().keySet()) {
				//HeapSort.heapSort(kindex.getKGramIndex().get(key));					//Joanne: 11/7/2012
				//test to see kgram index list (this portion can be taken out later)
				//System.out.println(key + ": " + kindex.getKGramIndex().get(key));
		//}
		
		//System.out.println("TOTAL UNIQUE GRAMS: " + kindex.getKGramIndex().size());
		//------------------------------------------------------------------------
	}
			
		
	private void indexDocuments() {
		for (Document document: docStore.getDocuments()) {
			indexDocument(document);
		}
	}
	

	private void indexDocument(Document document) {
		String[] tokens = document.getText().split(" ");
		// for each token found in the document
		for (int i = 0; i < tokens.length; i++) {
			String token = tokens[i];
			// convert the token to a type
			String stripped = StringUtils.stripToken(token);
			// if a token doens't contain at least one letter, ignore it
			if (!stripped.isEmpty()) { 
				//&& token.matches("\\w+.*")) {
				//System.out.println("[" + stripped + "]");
				// add kgrams to kgram index
				kindex.indexWord(stripped, 3);
				// add the stemmed term to our index
				index.addTerm(PorterAlgorithm.stripSuffixes(stripped), document.getId(), i);
			}
		}
	}	
}
