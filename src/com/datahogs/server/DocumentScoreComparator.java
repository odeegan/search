package com.datahogs.server;

import java.util.Comparator;

public class DocumentScoreComparator implements Comparator<PListDoc>{
    @Override
    public int compare(PListDoc pld1, PListDoc pld2) {
    	if (pld1.getScore() > pld2.getScore()) {
    		return -1;
    	} else if (pld1.getScore() < pld2.getScore()) {
    		return 1;
    	} else {
    		return 0;
    	}
    }
}