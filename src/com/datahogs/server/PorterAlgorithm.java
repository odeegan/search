package com.datahogs.server;



import java.util.regex.Matcher;
import java.util.regex.Pattern;

class NewString {
  public static String str;

  NewString() {
     str = "";
  }
}
    /**
     * @param args the command line arguments
     **/
public class PorterAlgorithm {

    /**---------------------------------------
     *           M E T H O D S
     *--------------------------------------**/     
  private static boolean hasSuffix( String word, String suffix, NewString stem ) {  
     Pattern p=Pattern.compile(suffix+"$");
     Matcher m=p.matcher(word);
     if(m.find())
     {
    	 //System.out.println(word.length());
        for (int i=0; i<word.length()-suffix.length(); i++){
            stem.str += word.charAt(i);
        }
        return true;
     }
     else
         return false;
  }
  
  private static boolean vowel( char ch, char prev ) {
     switch (ch) {
        case 'a': 
        case 'e': 
        case 'i': 
        case 'o': 
        case 'u': 
          return true;
        case 'y': {
          switch (prev) {
            case 'a': 
            case 'e': 
            case 'i': 
            case 'o': 
            case 'u': 
              return false;
            default: 
              return true;
          }
        }  
        default : 
          return false;
     }
  }

  private static int measure( String stem ) {   
    int i=0, count = 0;
    int length = stem.length();

    while (i < length) {
       //check for vowel
       for ( ; i < length; i++) {
           if (i > 0) {
              if (vowel(stem.charAt(i),stem.charAt(i-1)))
                 break;
           }
           else {  
              if (vowel(stem.charAt(i),'a'))
                break; 
           }
       }
       //check for consonant
       for (i++ ; i < length ; i++) {
           if (i > 0) {
              if (!vowel(stem.charAt(i),stem.charAt(i-1)))
                  break;
              }
           else {  
              if (!vowel(stem.charAt(i),'?'))
                 break;
           }
       } 
      //increment count when a VC pattern is found
      if (i < length) {
         count++;
         i++;
      }
    } //while
    
    return(count);
  }

  private static boolean containsVowel( String word ) {
     for (int i=0; i < word.length(); i++)
         if (i > 0) {
            if (vowel(word.charAt(i),word.charAt(i-1)))
               return true;
         }
         else {  
            if (vowel(word.charAt(0),'a'))
               return true;
         }       
     return false;
  }

  private static boolean cvc( String str ) {
     int length=str.length();

     if (length < 3)
        return false;
    
     if ((!vowel(str.charAt(length-1),str.charAt(length-2)))
        && (str.charAt(length-1) != 'w') && (str.charAt(length-1) != 'x') && (str.charAt(length-1) != 'y')
        && (vowel(str.charAt(length-2),str.charAt(length-3))) ) {

        if (length == 3) {
           if (!vowel(str.charAt(0),'?')) 
              return true;
           else
              return false;
        }
        else {
           if (!vowel(str.charAt(length-3),str.charAt(length-4))) 
              return true; 
           else
              return false;
        } 
     }    
     return false;
  }

  private static String step1(String word) {
 
     NewString stem = new NewString();
     //plural
     if (word.charAt(word.length()-1) == 's') {
        if (hasSuffix(word, "sses", stem) || hasSuffix(word, "ies", stem)){
           word = word.substring(0, word.length()-2);
        }
        else {
           if(word.length() == 1){
               return word;
           }
           
          //if ends with ss, do nothing
           if(word.charAt(word.length()-2) != 's') {
              word = word.substring(0, word.length()-1);
           }
        }  
     }
     //past tense
     if (hasSuffix(word,"eed",stem)) {
           if (measure(stem.str) > 0) {
              word = word.substring(0, word.length()-1);
           }
     }
     else {  
        if (hasSuffix(word,"ed",stem) || hasSuffix(word,"ing",stem)) { 
           if (containsVowel(stem.str)) {
              word = word.substring(0, stem.str.length());
              if (word.length() == 1)
                 return word;

              if (hasSuffix(word,"at",stem) || hasSuffix(word,"bl",stem) || (hasSuffix( word,"iz",stem))) {
                 word += "e";
              }
              else {   
                 int length = word.length(); 
                 if ((word.charAt(length-1) == word.charAt(length-2)) 
                    && (word.charAt(length-1)!= ('a'|'e'|'i'|'o'|'u'|'y')) 
                    && (word.charAt(length-1) != 'l') && (word.charAt(length-1) != 's') && (word.charAt(length-1) != 'z')) {                    
                    word = word.substring(0, word.length()-1);
                 }
                 else
                    if ((measure(word) == 1) && (cvc(word))) { 
                          word += "e";
                    }
              }
           }
        }
     }
     //ends with y
     if (hasSuffix(word,"y",stem) && containsVowel(stem.str)) {
           word =  word.substring(0, word.length()-1) + "i";
        }
     return word;  
  }

  private static String step2( String word ) {

     String[][] suffixes = { 
        { "ational", "ate" },
        { "tional",  "tion" },
        { "enci",    "ence" },
        { "anci",    "ance" },
        { "izer",    "ize" },
        { "iser",    "ize" },
        { "abli",    "able" },
        { "alli",    "al" },
        { "entli",   "ent" },
        { "eli",     "e" },
        { "ousli",   "ous" },
        { "ization", "ize" },
        { "isation", "ize" },
        { "ation",   "ate" },
        { "ator",    "ate" },
        { "alism",   "al" },
        { "iveness", "ive" },
        { "fulness", "ful" },
        { "ousness", "ous" },
        { "aliti",   "al" },
        { "iviti",   "ive" },
        { "biliti",  "ble" }
     };
    
     NewString stem = new NewString();   
     for (int index = 0; index < suffixes.length; index++) {
         if (hasSuffix(word, suffixes[index][0], stem)) {
            if (measure(stem.str) > 0) {
               word = stem.str + suffixes[index][1];
               return word;
            }
         }
     }
     return word;
  }

  private static String step3( String word ) {

        String[][] suffixes = { 
            { "icate", "ic" },
            { "ative", "" },
            { "alize", "al" },
            { "alise", "al" },
            { "iciti", "ic" },
            { "ical",  "ic" },
            { "ful",   "" },
            { "ness",  "" }
        };
        NewString stem = new NewString();

        for (int index = 0; index<suffixes.length; index++) {
            if (hasSuffix (word, suffixes[index][0], stem))
               if (measure(stem.str) > 0) {
                  word = stem.str + suffixes[index][1];
                  return word;
               }
        }
        return word;
  }

  private static String step4( String word ) {
        
     String[] suffixes = { 
         "al",      "ance",     "ence",     "er",   "ic",       "able",     "ible",
         "ant",     "ement",    "ment",     "ent",  "ion",       "ou",
         "ism",     "ate",      "iti",      "ous",  "ive",      "ize",      "ise"
     };
     
     NewString stem = new NewString();
        
     for (int index = 0 ; index<suffixes.length; index++) {
         if (hasSuffix(word, suffixes[index], stem)) {         
            if (measure(stem.str) >= 2) {
            	if (suffixes[index] == "ion" && 
            		(stem.str.endsWith("t") || stem.str.endsWith("s"))){
            		word = stem.str;
            		return word;
            	}
            }
         }
     }
     return word;
  }

  private static String step5( String word) {
     if (word.charAt(word.length()-1) == 'e') { 
        if (measure(word) >= 2) {
           word = word.substring(0, word.length()-1);
        }
        else
           if (measure(word) == 1) {
              String stem = "";
              stem = word.substring(0, word.length()-1);

              if (!cvc(stem))
                 word = stem;
           }
     }    
     
     if ((word.charAt(word.length()-1) == 'l') 
    	&& (word.charAt(word.length()-2) == 'l') 
    	&& (measure(word) >= 2) ){
           word = word.substring(0, word.length()-1);
        } 
     return word;
  }

  public static String stripSuffixes( String word ) {
	  //System.out.print("word: " + word);
	  if(word.length() > 1)
    	 word = step1( word );
    
     if ( word.length() > 1 )
        word = step2( word );
     
     if ( word.length() > 1 )
        word = step3( word );
     
     if ( word.length() > 1 )
        word = step4( word );
     
     if ( word.length() > 1 )
        word = step5( word );
	 //System.out.println("  stem: " + word);

     return word; 
  }
}
