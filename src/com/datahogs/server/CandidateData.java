package com.datahogs.server;

public class CandidateData {
	public int bestCase;
	public int worstCase;
	public int position;
	public int editDistance;
	
	public CandidateData() {}
	
	public CandidateData(int bc, int wc, int ps, int ed) {
		this.bestCase = bc;
		this.worstCase = wc;
		this.position = ps;
		this.editDistance = ed;
	}

	public void setBestCase(int bc) {
		bestCase=bc;
	}
	
	public int getBestCase() {
		return bestCase;
	}	

	public void incBestCase(){
		bestCase=bestCase+2;
	}
	
	public void setWorstCase(int wc) {
		worstCase=wc;
	}
	
	public void decWorstCase(){
		worstCase=worstCase-2;
	}
	public int getWorstCase() {
		return worstCase;
	}
	
	public void setPosition(int ps){
		position = ps;
	}
	public int getPosition(){
		return position;
	}
	public void setEditDistance(int ed){
		editDistance = ed;
	}
	public int getEditDistance(){
		return editDistance;
	}

}
