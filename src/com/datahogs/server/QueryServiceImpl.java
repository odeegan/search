package com.datahogs.server;

import java.util.ArrayList;
import java.util.Collections;

import com.datahogs.client.QueryService;
import com.datahogs.shared.Result;
import com.datahogs.shared.ResultsContainer;
import com.datahogs.shared.SpellCorrectionCandidate;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class QueryServiceImpl extends RemoteServiceServlet implements
		QueryService {

	Index index = null;
	KIndex kindex = null;
	Indexer indexer;
	String queryProcessingTime;
	//PList plist = null;
	PList completeList = null;
	int currPos = 0;
	int prevPos = 0;
	
	
	public QueryServiceImpl() {
		System.out.println("Creating the query service");
	}

	@Override
	public String getBlobstoreUploadUrl() {
	    BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
	    return blobstoreService.createUploadUrl("/upload");
	}
	
	public String createIndex() {
	    System.out.println("Begin indexing documents");
		long startTime = System.currentTimeMillis();
		Indexer indexer = new Indexer(DocStore.getInstance(), Index.getInstance(), KIndex.getInstance()); 
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;
		return  Double.toString(duration / 1000.00);
	}
	
	public String getDocumentTextById(int id) {
		Document doc = DocStore.getInstance().getDocument(id);
		return doc.getText();
	}

	
	public ArrayList<SpellCorrectionCandidate> suggestSpelling(String term) {		
		ArrayList<SpellCorrectionCandidate> results = new ArrayList<SpellCorrectionCandidate>();
		// use EditDistanceCandidateSelector class to find a list of candidates
		// calculate their edit distance from the term
		EditDistanceCandidateSelector candidateList = new EditDistanceCandidateSelector();
		candidateList.CalculateCandidateList(term, 2);
		//for(String i: candidateList.getCandidateList().keySet()){
		//Collections.sort(candidateList.getCandidateList().get(i).editDistance, new CandidateListComparator());
		//}
		//candidateList.getCandidateList().keySet();
		// create a SpellCorrectionCandidate object for each candidate
		SpellCorrectionCandidate one;

		for(String c: candidateList.getCandidateList().keySet()){	
			System.out.println(EditDistance.editDistanceCount(term, c));
			one = new SpellCorrectionCandidate();
			one.setTerm(c);
			one.setEditDistance(EditDistance.editDistanceCount(term, c));
			results.add(one);
		}

		// setTerm equal to candidate
		//one.setTerm("wordy");
		// set computed edit distance for term
		//one.setEditDistance(EditDistance.editDistanceCount(term, "wordy"));
		// add to results
		//results.add(one);
		Collections.sort(results, new CandidateListComparator());
		return results;
	}
	
	
	public ResultsContainer queryServer(String input) {

		/*
		 * a Query is a list of Qs
		 * a Q is a list of QueryLiterals
		 * a QueryLiteral contains the query literal as a string
		 * a QueryLiteral also knows if it is negative or if  
		 *    it represents an exact phrase match
		 */
		PList plist = null;
		Query query;
		currPos = 0;
		prevPos = 0;
		long startTime = System.currentTimeMillis();
		 // check if it's a wildcard query
		if (input.contains("*")) {
			String expandedQueryString = WildCardQueryExpander.expand(input);
			System.out.println(WildCardQueryExpander.expand(input));
			query = new Query(expandedQueryString);	
		} else {
			query = new Query(input);		
		}

		plist = query.getPostingsList();			
		
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;	
		queryProcessingTime = Double.toString(duration / 1000.00);

		// sort the results by their term weighted scores		
		Collections.sort(plist.getDocs(), new DocumentScoreComparator());
		completeList = plist.deepCopy();

		ResultsContainer rc;
		if (plist.getSize() <= 15) {
			prevPos = currPos;
			currPos = plist.getSize();
			rc = getResults(plist, query.toString());
		} else {
			prevPos = currPos;
			currPos = currPos + 15;
			rc = getResults(plist.subList(0, 15), query.toString());
		}
		return rc;
	}

	
	/*
	 * get the next set of results
	 */
	public ResultsContainer next() {
		ResultsContainer rc;
		if (currPos + 15 > completeList.getSize() ) {
			prevPos = currPos;
			currPos = completeList.getSize();
			rc = getResults(completeList.subList(prevPos, currPos), "");
		} else {
			prevPos = currPos;
			currPos = currPos + 15;
			rc = getResults(completeList.subList(prevPos, currPos), "");
		}
		return rc; 
	}

	/*
	 * get the previous set of results
	 */
	public ResultsContainer previous() {
		ResultsContainer rc;
		currPos = prevPos;
		prevPos = prevPos - 15;
		rc = getResults(completeList.subList(prevPos, currPos), "");
		return rc;
	}
	
	
	
	/*
	 *@return ArrayList of Result objects
	 *
	 *@gwt.typeArgs <client.ResultsContainer>
	 */
	public ResultsContainer getResults(PList list, String query) {
		ResultsContainer resultsContainer = new ResultsContainer();
		resultsContainer.setCurrPos(currPos);
		resultsContainer.setPrevPos(prevPos);
		resultsContainer.setQuery(query);
		resultsContainer.setTotal(completeList.getSize());
		
		Result result;
		
		for (PListDoc doc:list.getDocs()) {
			result = new Result();
			result.setId(doc.id);
			result.setScore(doc.getScore());
			result.setSnippet(DocStore.getInstance().getDocument(doc.id).getSnippet());
			resultsContainer.addResult(result);
		}
		resultsContainer.setQueryProcessingTime(queryProcessingTime);
		return resultsContainer;
	}
	
}
