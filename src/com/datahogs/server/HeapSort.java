package com.datahogs.server;

import java.util.List;

//reference: http://rosettacode.org/wiki/Sorting_algorithms/Heapsort#Java

public class HeapSort {

	// sort a list of strings using heap sort
	public static void heapSort(List<String> a){
		int count = a.size();
	 
		//first place the list in max-heap order
		heapify(a, count);
	 
		int end = count - 1;
		while(end > 0){
			
			String tmp = a.get(end);		//swap the root element with the last element of heap
			a.set(end, a.get(0));
			a.set(0, tmp);
			siftDown(a, 0, end - 1);		//put heap back in max-heap order
			end--;							//decrement the size of the heap
		}
	}
	 
	// build a max-order heap by sifting down the root. 
	public static void heapify(List<String> a, int count){
		//start = index of last parent node
		int start = (count - 2) / 2;
	 
		while(start >= 0){
			siftDown(a, start, count - 1);
			start--;
		}
	}
	 
	// Sifts down the root of the binary tree and and exchange the root value
	// with the maximum value of left child compared to right child if the root value is smaller
	// than that child's max value.
	public static void siftDown(List<String> a, int start, int end){
		int root = start;
	 
		while((root * 2 + 1) <= end){      							//While the root has at least one child
			int child = root * 2 + 1;          						//left child
			//if the child has a sibling and the child's value is less than its sibling's...
			if(child + 1 <= end && a.get(child).compareTo(a.get(child + 1)) < 0)
				child = child + 1;           						// pick the right child 
			if(a.get(root).compareTo(a.get(child)) < 0){     		// exchange values if root is less than child
				String tmp = a.get(root);
				a.set(root, a.get(child));
				a.set(child, tmp);
				root = child;                						//repeat to continue sifting down the child now
			}else
				return;
		}
	}
}
