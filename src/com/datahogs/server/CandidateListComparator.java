package com.datahogs.server;

import java.util.ArrayList;
import java.util.Comparator;

import com.datahogs.shared.SpellCorrectionCandidate;


public class CandidateListComparator implements Comparator<SpellCorrectionCandidate>{
		@Override
	    public int compare(SpellCorrectionCandidate w1, SpellCorrectionCandidate w2) {
	    	if (w1.getEditDistance() < w2.getEditDistance()) {
	    		return -1;
	    	} else {
	    		return 1;
	    	}
	    }
}
