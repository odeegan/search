package com.datahogs.server;

import java.util.ArrayList;
import java.util.Arrays;


public class QueryLiteral {

	private String queryLiteralString;
	private Boolean isNegative = false;
	private Boolean isExactPhrase = false;

	private PList postingsList;
	
	private ArrayList<String> terms = new ArrayList<String>();
	
	public QueryLiteral(String qls) {
		queryLiteralString = qls;
		// determine if the QueryLiteral is an exact phrase match
		// if it has a quote in the string
		if (queryLiteralString.startsWith("-")) {
			this.setNegative(true);
			// remove the - character
			queryLiteralString = queryLiteralString.substring(1, queryLiteralString.length());
		}
		
		if (queryLiteralString.startsWith("\"")) {
			this.setExactPhrase(true);
			//remove quotes
			queryLiteralString = queryLiteralString.substring(1, queryLiteralString.length()-1);
			// splits string on " "
			// converts String[] to ArrayList<String>
			ArrayList<String> unstemmed = new ArrayList<String>(Arrays.asList(queryLiteralString.split(" ")));
			for (String term: unstemmed) {
				terms.add(PorterAlgorithm.stripSuffixes(term));
			}
		} else {
			// stem the term and add them to our list
			terms.add(PorterAlgorithm.stripSuffixes(queryLiteralString));
		}
		
		
		System.out.println("      |--isNegative: " + isNegative());
		System.out.println("      |--isExactPhrase: " + isExactPhrase());		
		for (String term: terms) {
			System.out.println("      |--Term: " + "'" + term + "'");			
		}
	}
	
	
	public PList getPostingsList() {
		if (!(postingsList == null)) {
			return  postingsList;
		} else {
			// QueryLiteral contains a single term
			if (terms.size() == 1) {
				// grab the postingsList and return it
				postingsList = Index.getInstance().getPostingsList(terms.get(0));
				//System.out.println("TERM: " + terms.get(0) + "\n" + postingsList);
				// QueryLiteral is an exact phrase match
			} else {
				//System.out.println("exact phrase match");
				postingsList = Index.getInstance().getPostingsList(terms.get(0));
				//System.out.println("TERM: " + terms.get(0) + postingsList);
				// perform a positional intersect on the terms in the exact phrase match
				for (int i = 1; i < terms.size(); i++) {		
					//System.out.println("TERM: " + terms.get(i) + Index.getInstance().getPostingsList(terms.get(i)));
					postingsList = postingsList.positionalIntersect(Index.getInstance().getPostingsList(terms.get(i)), i);
				}
			}
		}
		//System.out.println("\nQUERYLITERAL POSTINGSLIST \n" + postingsList);
		return postingsList;
	}
	
	public ArrayList<String> getTerms() {
		return terms;
	}
	
	private void setNegative(Boolean bool) {
		isNegative = bool;
	}

	public Boolean isNegative() {
		return isNegative;
	}

	private void setExactPhrase(Boolean bool) {
		isExactPhrase = bool;
	}
	
	public Boolean isExactPhrase() {
		return isExactPhrase;
	}

	public String toString() {
		return queryLiteralString;
	}
	
}
