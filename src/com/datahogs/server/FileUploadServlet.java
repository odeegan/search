package com.datahogs.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreInputStream;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;



public class FileUploadServlet extends HttpServlet{
		 
	private BlobstoreService blobstoreService = BlobstoreServiceFactory
	           .getBlobstoreService();
	 
	public void doPost(HttpServletRequest req, HttpServletResponse res)
	        throws ServletException, IOException {
	 
		// clear the indexes
		Index.getInstance().reset();
		KIndex.getInstance().reset();
		DocStore.getInstance().reset();

		
	    Map<String,List<BlobKey>> blobs = blobstoreService.getUploads(req);
	    List<BlobKey> bkeys = blobs.get("file");
		String line;
	    if (bkeys.size() > 1) {
    		// multiple documents	    	
	   		for (BlobKey b: bkeys) {
        		System.out.println("multiple files");
    			InputStream is = new BlobstoreInputStream(b);
        		BufferedReader br = new BufferedReader( new InputStreamReader(is));
        		StringBuilder sb = new StringBuilder();
    	    	
    	    	while ((line = br.readLine()) != null) {
        			if(line.matches("")) {
                        DocStore.getInstance().addDocument(sb.toString());
                        sb = new StringBuilder();
                    } else {
                        sb.append(" " + line);
                    }
    	    	}
        		DocStore.getInstance().addDocument(sb.toString());
        		br.close();
    		}
    	} else {
    		// multiple documents	    	
    		System.out.println("single files");
    		InputStream is = new BlobstoreInputStream(bkeys.get(0));
    		BufferedReader br = new BufferedReader( new InputStreamReader(is));
    		StringBuilder sb = new StringBuilder();

	    	while ((line = br.readLine()) != null) {
    			if(line.matches("")) {
                    DocStore.getInstance().addDocument(sb.toString());
                    sb = new StringBuilder();
                } else {
                    sb.append(" " + line);
                }
	    	}
    		DocStore.getInstance().addDocument(sb.toString());
    		br.close();
        }
    	Index.getInstance().setNumberOfDocumentsIndex(DocStore.getInstance().getCount());
    	System.out.println("Found " + Index.getInstance().getNumberofDocumentsIndex() + " documents");
    	for (BlobKey key:bkeys) {
    		System.out.println("removing blob " + key.toString());
    		blobstoreService.delete(key);
    	}
	}
		

    @Override
	    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
	            throws ServletException, IOException {
	 
	        //String imageUrl = req.getParameter("imageUrl");
	        //resp.setHeader("Content-Type", "text/html");
	 
	        // This is a bit hacky, but it'll work. We'll use this key in an Async
	        // service to
	        // fetch the image and image information
	        resp.getWriter().println("supposed to be a header");
	 
	    }
}
	

