package com.datahogs.server;
import java.util.ArrayList;
import java.util.Iterator;

/*
 * PostingsListDocuments contain:
 *  - document id
 *  - positions of the term in the doc
 *  - the term frequency in the document (size of the positions list) 
 *  - score of the document with respect to the query
 */
public class PListDoc {
	
	public int id;
	public ArrayList<Integer> positions = new ArrayList<Integer>();
	public double score;
	
	
	public PListDoc() {}
	
	public PListDoc(int docId, int position) {
		this.id = docId;
		positions.add(position);
	}

	public void setId(int i) {
		id = i;
	}
	
	public Integer getId() {
		return id;
	}	

	public Integer get(Integer i) {
		return positions.get(i);
	}
	
	public void add(Integer i) {
		positions.add(i);
	}
		
	public Integer getTermFrequency() {
		return positions.size();
	}
	
	public void setScore(double i) {
		score = i;
	}
	
	public double getScore() {
		return score;
	}

}
