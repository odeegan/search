package com.datahogs.server;

import java.util.ArrayList;
import java.util.List;

public class DocStore {

	
	private static final DocStore docStore = new DocStore();
	private ArrayList<Document> docs = new ArrayList<Document>();
	private int docCount = 0;
	
	private DocStore() {}
	
	public static DocStore getInstance() { return docStore; }
	
	public void addDocument(String text) {
		docs.add(new Document(docCount, text));
		if (text.length() > 200) {
			docs.get(docCount).setSnippet(text.substring(1, 200) + " ...");
		} else {
			docs.get(docCount).setSnippet(text);
		}
		docCount = docCount + 1;
	}
	
	public Document getDocument(int id) {
		// TODO: add error checking
		return docs.get(id);
	}
	
	public ArrayList<Document> getDocuments() {
		return docs;
	}
	
	public int getCount() {
		return docCount;
	}
	
	public void reset() {
		docs.clear();
		docCount = 0;
	}
	
}
