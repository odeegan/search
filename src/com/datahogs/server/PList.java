package com.datahogs.server;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;


public class PList {

	public ArrayList<PListDoc> docs = new ArrayList<PListDoc>();
	
	
	public PList() {}
		
	public PListDoc get(int i) {
		return docs.get(i);
	}
	
	public void add(PListDoc d) {
		docs.add(d);
	}

	public ArrayList<PListDoc> getDocs() {
		return docs;
	}
	
	public PListDoc getDocById(int docId) {
		PListDoc doc = null;
		for (PListDoc pld: docs) {
			if (pld.getId() == docId) {
				doc = pld;
				break;
			}
		}
		if (doc == null) {
			doc = new PListDoc();
		}
		return doc;
	}
	
	/*
	 * returns true if the PList has the docID
	 */
	public boolean contains(int docID) {
		boolean hasDoc = false;
		for (PListDoc doc: docs) {
			if (doc.getId() == docID) {
				hasDoc = true;
			}
		}
		return hasDoc;
	}
	
	public int getSize() {
		//System.out.println("getting size");
		return docs.size();
	}
		
	
	public PList deepCopy() {
		PList listCopy = new PList();
		PListDoc docCopy;
		for (PListDoc doc: this.getDocs()) {
			docCopy = new PListDoc();
			docCopy.setId(doc.getId());
			docCopy.setScore(doc.getScore());
			listCopy.add(docCopy);
		}
		return listCopy;
	}
	
	public PList subList(int start, int finish) {
		PList subList = new PList();
		PListDoc docCopy;
		for (int i = start; i < finish; i++) {
			docCopy = new PListDoc();
			docCopy.setId(this.get(i).getId());
			docCopy.setScore(this.get(i).getScore());
			subList.add(docCopy);
		}
		return subList;
	}
	
	public PList merge(PList listB) {
		//System.out.println("Merge");

		PList listC = new PList();
		PList listA = this;
		
		ArrayList<PListDoc> docListA = listA.getDocs();		
		ArrayList<PListDoc> docListB = listB.getDocs();
		
		// short circuit if one of the lists is null
		// and just return the other list
		if (docListA.size() == 0) {
			for (PListDoc doc: docListB) {
				listC.add(doc);
			}
			return listC;
		} else if (docListB.size() == 0) {
			for (PListDoc doc: docListA) {
				listC.add(doc);
			}
			return listC;
		}
		
		
		Integer a = 0;
		Integer b = 0;

		PListDoc pld;
		
		while (true) {
			if (a == docListA.size()) {
				for (PListDoc doc: docListB.subList(b, docListB.size())) {
					listC.add(doc);
				}
				//System.out.println("\nRESULT \n" + listC);
				return listC;
			} else if (b == docListB.size()) {
				for (PListDoc doc: docListA.subList(a, docListA.size())) {
					listC.add(doc);
				}				
				//System.out.println("\nRESULT \n" + listC);
				return listC;
			}
					
			if (docListA.get(a).id == docListB.get(b).id) {
				////pld = new PListDoc();
				////pld.id = docListA.get(a).id;
				////listC.add(pld);
				listC.add(docListA.get(a));
				a = a + 1;
				b = b + 1;
			} else if (docListA.get(a).id < docListB.get(b).id) {
				////pld = new PListDoc();
				////pld.id = docListA.get(a).id;
				////listC.add(pld);
				listC.add(docListA.get(a));
				a = a + 1;
			} else {
				//pld = new PListDoc();
				//pld.id = docListB.get(b).id;
				//listC.add(pld);
				listC.add(docListB.get(b));
				b = b + 1;			
			}
		}
	}
	
	
	
	public PList intersect(PList listB, Boolean operator) {
		System.out.println("Intersecting");

		PList listC = new PList();
		PList listA = this;

		ArrayList<PListDoc> docListA = listA.getDocs();		
		ArrayList<PListDoc> docListB = listB.getDocs();
				
		Integer a = 0;
		Integer b = 0;

		PListDoc pld;
		
		while (a < docListA.size() && b < docListB.size()) {
			//System.out.println("a="+ a + "   b=" + b);					
			//System.out.println("A.id="+ docListA.get(a).id + "   B.id=" + docListB.get(b).id);		
			if (docListA.get(a).id == docListB.get(b).id) {
				//System.out.println("doc ids match");
				if (operator) {
					//System.out.println("adding the doc [==]");					
					////pld = new PListDoc();
					////pld.id = docListA.get(a).id;
					//pld.positions.addAll(docListA.get(a).positions);
					//pld.positions.addAll(docListB.get(b).positions);
					////Collections.sort(pld.positions);
					////listC.add(pld);
					listC.add(docListA.get(a));
				}
				a = a + 1;
				b = b + 1;
			} else if (docListA.get(a).id < docListB.get(b).id) {
				if (!operator) {
					//System.out.println("adding the doc [A<B]");					
					////pld = new PListDoc();
					////pld.id = docListA.get(a).id;					
					//pld.positions.addAll(docListA.get(a).positions);
					////listC.add(pld);
					listC.add(docListA.get(a));					
				}
				a = a + 1;
			} else {
				/*
				 * removed this earlier, but not sure why
				 */
				//if (!operator) {
				//	pld = new PListDoc();
				//	pld.id = docListA.get(a).id;					
					//pld.positions.addAll(docListA.get(a).positions);
				//	listC.add(pld);
				//}
				b = b + 1;
			}
		}
		
		// if we're intersecting a positve and a negative query literal
		// add all the remaining positve query literal docs to the result
		if (!operator) {
			if (a < docListA.size()) {
				while (a < docListA.size()) {
					////pld = new PListDoc();
					////pld.id = docListA.get(a).id;
					////listC.add(pld);
					listC.add(docListA.get(a));
					a = a + 1;
				}
			}
		}
		
		return listC;
	}
	
	
	public PList positionalIntersect(PList listB, int offset) {
		//System.out.println("PIntersect");
		// listC is the intersection list
		PList listC = new PList();
		PList listA = this;
		
		ArrayList<PListDoc> docListA = listA.getDocs();		
		ArrayList<PListDoc> docListB = listB.getDocs();
		
		Integer a = 0;
		Integer b = 0;
		
		ArrayList<Integer> positionsA;
		ArrayList<Integer> positionsB;
		
		Integer aPos = 0;
		Integer bPos = 0;
		
		PListDoc pld;
		
		while (a < docListA.size() && b < docListB.size()) {
			//System.out.println("A.id="+ docListA.get(a).id + "   B.id=" + docListB.get(b).id);		
			if (docListA.get(a).id == docListB.get(b).id) {
				//System.out.println("doc ids match");
				positionsA = docListA.get(a).positions;
				positionsB = docListB.get(b).positions;
				pld = new PListDoc();
				pld.id = docListA.get(a).id;
				aPos = 0;
				bPos = 0;
				while (aPos < positionsA.size() && bPos < positionsB.size()) {
					//System.out.println("positionA="+ positionsA.get(aPos) + "   positionB=" + positionsB.get(bPos));
					if(positionsA.get(aPos) == positionsB.get(bPos) - offset) {
						//System.out.println("a come right after b: " + positionsA.get(aPos) + " --> " + positionsB.get(bPos));
						pld.positions.add(positionsA.get(aPos));
						aPos = aPos + 1;
						bPos = bPos + 1;
					} else if (positionsA.get(aPos) < positionsB.get(bPos) - offset) {
						aPos = aPos + 1;
					} else {
						bPos = bPos + 1;
					}
				}
				if (pld.positions.size() > 0) {
					listC.add(pld);
				}
				a = a + 1;
				b = b + 1;
			} else if (docListA.get(a).id < docListB.get(b).id) {
				a = a + 1;
			} else {
				b = b + 1;
			}
		}
		
		return listC;
	}
	
	
	public String toString() {
		String str = "";
		for (PListDoc doc: docs) {
			str += "\nID: " + doc.id + "  ";
			str += "POSITIONS: " + doc.positions;
		}
		return str;
	}	
}
