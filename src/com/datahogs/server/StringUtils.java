package com.datahogs.server;

import java.util.List;

public class StringUtils {
				
		/* 
		 * 0. split tokens on white space
		 * 1. remove punctuation
		 * 		- exclamation points
		 * 		- single and double quotes
		 * 		- periods
		 * 		- semicolons
		 * 		- colons
		 * 		- commas
		 * 		- question marks
		 * 		- underscore		Joanne: 10/20/12
		 * 		- left parenthesis	Joanne: 10/20/12
		 * 		- lowercase
		 * 		- stem the token
		 * 3. 
		 */
	public static String stripToken(String token) {
		String stripped;
		stripped = token.replaceAll("[?,;:!.\"_()' ]", "");
		return stripped.toLowerCase();
	}
	
	
	public static String join(List<String> list, String delimeter) {
		String s = "";
		if (list.size() == 1) {
			s =  list.get(0);
		} else {
			for (int i=0; i < list.size() - 1; i++) {
				s = s + list.get(i) + " " + delimeter + " ";
			}
			s = s + list.get(list.size() - 1);
		}
		return s;		
	}
}
