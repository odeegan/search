package com.datahogs.server;

import java.util.Comparator;

public class QueryLiteralComparator implements Comparator<QueryLiteral>{
    @Override
    public int compare(QueryLiteral ql1, QueryLiteral ql2) {
    	if (ql1.isNegative() && ql2.isNegative()) {
    		return 0;
    	} else if (ql1.isNegative() && !ql2.isNegative())
    		return 1;
    	else {
    		return -1;
    	}
    }
}
