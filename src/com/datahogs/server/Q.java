package com.datahogs.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Q {

	private ArrayList<QueryLiteral> queryLiterals = new ArrayList<QueryLiteral>();
	
	public Q(String qString) {
		List<String> matchList = new ArrayList<String>();
		// split into QueryLiterals by splitting on white space, but
		// preserve white spaces in quoted strings
		Pattern regex = Pattern.compile("-?\"[^\"]*\"|-?'[^']*'|[^\\s\"']+");
		Matcher regexMatcher = regex.matcher(qString);
		while (regexMatcher.find()) {
		    matchList.add(regexMatcher.group());
		} 	
		
		for (String match: matchList) {
			System.out.println("  |--QueryLiteral: " + "'" + match + "'");
			queryLiterals.add(new QueryLiteral(match));
		}
	}
	
	/*
	 * This sorts the QueryLiterals to make sure any negative
	 * query literals are processed after the positives.
	 */ 
	public ArrayList<QueryLiteral> getQueryLiterals() {		
		Collections.sort(queryLiterals, new QueryLiteralComparator());
		return queryLiterals;
	}
	
	public PList getPostingsList() {
		PList result = null;
		if (getQueryLiterals().isEmpty()) {
			result = new PList();
		} else {
			PList plist = null;
			// intersect each of the query literals
			// make sure the first one we grab is a positive query literal
			result = getQueryLiterals().get(0).getPostingsList();
			//System.out.println(queryLiterals.get(0).getTerms());
			for (int i = 1; i < getQueryLiterals().size(); i++) {
				// is the query literal negative?
				Boolean operator = getQueryLiterals().get(i).isNegative();
				// get the postings list from the query literal
				//System.out.println(queryLiterals.get(i).getTerms());
				plist = getQueryLiterals().get(i).getPostingsList();
				result = result.intersect(plist, !operator);
			}
		}
		return result;
	}
	
	
	
}
