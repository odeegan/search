package com.datahogs.server;

public class EditDistance {


	//compares two integer values and 
	// returns the smallest value
	public static int min(int dist1, int dist2)
	{
		if(dist1 < dist2)
			return dist1;
		else
			return dist2;
	}
	//	function: editDistanceCount
	//	parameters: string1 and string2
	//	return value: number of edits needed	
	public static int editDistanceCount(String s1, String s2)
	{
		int m = s1.length();
		int n = s2.length();
		int v[][] = new int[m+1][n+1];
		
		/*sets initial values vertically
		0
		1
		2
		3
		.
		.
		n
		*/
		for(int i=0; i<=m; i++)
		{
			v[i][0] = i;
		}
		/*sets initial values horizontally
		0 1 2 3 4 5 6 .. n
		*/
		for(int j=0; j<=n;j++)
		{
			v[0][j] = j;
		}
		
		//compare char of string 1 with string 2
		// if equal, edit count stays the same
		// if not equal, compares left, up, and diagonal entries
		// to find which is the min edit distance count and then
		// adds 1 to it. Returns the edit distance count
		for(int i=1; i<=m; i++)
		{
			for(int j=1; j<=n; j++)
			{
				if(s1.charAt(i-1) == s2.charAt(j-1))
					v[i][j]=v[i-1][j-1];
				else
					v[i][j]=1 + min( min ( v[i][j-1], v[i-1][j] ), v[i-1][j-1]);
			}
		}

		return v[m][n];
	}

}
