package com.datahogs.server;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WildCardQueryExpander {

	public WildCardQueryExpander(){}
	
	public static String expand(String query) {
		List<String> grams = getGrams(query);
		HashSet<String> candidateSet = new HashSet<String>();
		List<String> termList = new ArrayList<String>();
		for (String gram: grams) {
			for (String candidate: KIndex.getInstance().getKGramIndex().get(gram)) {
				candidateSet.add(candidate);			
			}
		}

		for (String candidate: candidateSet) {
			//System.out.println("candidate: " + candidate);			
			String matcher = query.replaceAll("\\*", ".*");
			if (candidate.matches(matcher)) {
				termList.add(candidate);
			}
		}
		return StringUtils.join(termList, "+");
	}
	
	/*
	 * return minimal set of non-overlapping kgrams
	 */
	private static ArrayList<String> getGrams(String s) {
		Set<String> gramSet = new HashSet<String>();

		String gram = "$";
		int ptr = 0;
		while (ptr < s.length()) {
			if (s.substring(ptr, ptr + 1).equals("*")) {
				ptr = ptr + 1;
				if (gram.matches("(\\$){0,1}[a-zA-Z]+")) {
					gramSet.add(gram);
					//System.out.println("adding current gram [*]: " + gram);
					gram = new String();
					continue;
				}
			}
			
			gram = gram + s.substring(ptr, ptr + 1);
			ptr = ptr + 1;

			if (gram.length() == 3) {
				//System.out.println("adding current gram [length == 2]: " + gram);
				gramSet.add(gram);
				gram = new String();
			}
			if (ptr == s.length() && gram.length() !=0) {
				gram = gram + "$";
				//System.out.println("adding current gram [last char]: " + gram);
				gramSet.add(gram);
			}
		}
		ArrayList<String> gramList = new ArrayList<String>();
		gramList.addAll(gramSet);
		//System.out.println(gramSet.toString());
		return gramList;
	}
	
	
}
